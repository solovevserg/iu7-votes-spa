# iu7-votes-spa

☝️ Итак, по итогам вчерашнего занятия мы с вами создали простенькое SPA-приложения для голосования с динамической подгрузкой результатов. На практике закрепили работу с CSS+HTML+JS.

Спасибо за активное участие, ведь благодаря вашему вниманию ещё в процессе написания кода было отловлено немало ошибок)

Приложение всё ещё развернуто на 👉 http://sdal.pw:7201/ (если у вас в кеше старая версия, сделайте принудительное обновление страницы)
Сейчас там бардак, но 10 минут в соответствии с заявленными требованиями оно проработало почти корректно.

✅ Код с небольшими дополнениями (css-переходы, тёмная тема, ...) и детальными комментариями доступен [тут](https://gitlab.com/solovevserg/iu7-votes-spa)
 Спасибо @kateorom приложение теперь выглядит не так вырвиглазно.

✅ Бэкенд на TypeScript можно глянуть тут
https://gitlab.com/solovevserg/iu7-votes

Источники
❕1️⃣ DOM работа со страницей из JS 
https://developer.mozilla.org/ru/docs/DOM

❕2️⃣ Снова флекс-боксы - ваш обоюдоострый нож для вёрстки
https://tproger.ru/translations/how-css-flexbox-works/


И ещё кое-что:
* ➡️пользовательские свойства в CSS
https://developer.mozilla.org/ru/docs/Web/CSS/Using_CSS_custom_properties
* ➡️ fetch API для отправки асинхронных запросов
https://developer.mozilla.org/ru/docs/Web/API/Fetch_API/Using_Fetch
* ➡️ CORS (Cross-Origin Resource Sharing)
https://developer.mozilla.org/ru/docs/Web/HTTP/CORS
* ➡️ Деструктуризация в JS
https://learn.javascript.ru/destructuring
* ➡️ CDN как способ подключения JS библиотек
https://ru.wikipedia.org/wiki/Content_Delivery_Network
