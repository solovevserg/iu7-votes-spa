// const BACKEND_ORIGIN = 'http://localhost:3000'
const BACKEND_ORIGIN = 'http://sdal.pw:7200'

// Создадим кастомную обёртку над стандартным вызовом фетч,
// которая учитывает наш ориджин и десериализует json из тела ответа
async function fetchJson(path, method = 'GET', body) {
    const url = `${BACKEND_ORIGIN}${path}`;
    return await fetch(url, {
        method,
        body,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(res => res.json());
}

// Методы АПИ в одном объекте для удобства
const api = {
    async getVotes() {
        return await fetchJson('/api/votes')
    },
    async getVote(deviceId) {
        return await fetchJson(`/api/votes/${deviceId}`)
    },
    async postVote(vote) {
        const body = JSON.stringify(vote);
        return await fetchJson('/api/votes', 'POST', body);
    }
}

// Тут соберём все используемые нами элементы из DOM
const elements = {
    voteForm: document.querySelector('#vote-form'),
    submitButton: document.querySelector('#submit'),
    formView: document.querySelector('#form-view'),
    resultView: document.querySelector('#result-view'),
    langsList: document.querySelector('#lang-list'),
    presetsButtons: document.querySelectorAll('.lang-preset'),
    langInput: document.querySelector('input[name="language"]'),
    themeToggleButton: document.querySelector('#theme-toggle'),
    body: document.body
}

// Набор шаблонов - функций, которые преобразуют JS-объект в HTML-код
const templates = {
    studentsList(votes) {
        return votes.map(vote => `<b>${vote.fullName}</b> ${vote.group}`).join(', ');
    },
    langDiv(lang) {
        return `<div class="lang">
                    <div class="lang-title" style="font-size: ${1 + lang.percent / 20}em;">${lang.name}</div>
                        <div class="bar-wrapper">
                            <div class="bar" style="width: ${lang.percent}%;">
                                ${lang.percent.toFixed(2)}%
                            </div>
                        </div>
                            ${this.studentsList(lang.votes)}
                    </div>
                </div >
                    `;
    },
    langAllDivs(langs) {
        return langs.map(lang => this.langDiv(lang)).join('');
    }
}

// функция забирает данные из формы ввода и возвращает объект с этими данными
function getVoteFormData() {
    // тут вот так [['language', '...'], ['fullName', '...']]
    const keyValuePairs = new FormData(elements.voteForm);
    // а теперь так {language: '...', fullName: '...'}
    const formObject = Object.fromEntries(keyValuePairs);
    return formObject;
}

function getDeviceId() {
    const key = 'deviceId';
    let deviceId = localStorage.getItem(key);
    if (!deviceId) {
        // UNIX timestamp + random number = unique id
        deviceId = `${new Date().getTime()}${Math.random()}`;
        localStorage.setItem(key, deviceId);
    }
    return deviceId;
}

async function loadVoteForm() {
    // Прячем то, что не надо, отображаем то, что надо.
    elements.formView.style.display = 'block';
    elements.resultView.style.display = 'none';
    // Обработчик на нажатие кнопки отправки формы
    elements.submitButton.addEventListener('click', async (event) => {
        console.log(event);
        const data = getVoteFormData();
        vote = {
            // в vote будут все поля из data и ещё немного
            ...data,
            time: new Date().getTime(),
            deviceId: getDeviceId()
        }
        await api.postVote(vote);
        // Отправили, а теперь переходим на вьюху с просмотром результатов
        loadLangsList();
    });

    // Добавляем обработчики на нажатия кнопок с названиями языков
    elements.presetsButtons.forEach(btn => {
        btn.addEventListener('click', event => {
            // эти два метода позволяют предотвратить отправку формы по нажатию на кнопку,
            // несмотря на то, что она находится внутри
            event.preventDefault();
            event.stopPropagation();
            // event.target - DOM-элемент, на котором сработало событие
            elements.langInput.value = event.target.textContent;
        })
    })
}

// сгруппируем голоса по языкам и отсортируем по популярности
function groupLangs(votes) {
    // Тут используется библиотека lodash - аналог LINQ в JS.
    // Чтобы это работало, надо подключить библиотеку.
    // Мы сделали это в index.html
    const grouped = _.groupBy(votes, 'language');
    const langs = Object.entries(grouped).map(([name, langVotes]) => ({
        name,
        votes: langVotes,
        percent: (langVotes.length / votes.length) * 100
    }));
    return _.orderBy(langs, 'percent', 'desc');
}

async function loadLangsList() {
    elements.formView.style.display = 'none';
    elements.resultView.style.display = 'block';
    // Пример создания вложенной функции
    async function refreshResults() {
        const votes = await api.getVotes();
        const langs = groupLangs(votes);
        const langsHTML = templates.langAllDivs(langs);
        elements.langsList.innerHTML = langsHTML;
    }
    // Тут же выводим свежие результаты
    refreshResults();
    // А также поставим таймер, который каждые 2500ms будет
    // вызывать функцию заново
    setInterval(refreshResults, 2500);

}

// акивируем кнопку для переключения темы приложения
function activateThemesToggle() {
    elements.themeToggleButton.addEventListener('click', () => {
        const darkTheme = 'dark'
        const isDark = elements.body.classList.contains(darkTheme);
        if (isDark) {
            elements.body.classList.remove(darkTheme)
            themeToggleButton.innerHTML = 'Тёмная тема';
        } else {
            elements.body.classList.add(darkTheme)
            themeToggleButton.innerHTML = 'Светлая тема';
        }
    });
}

// Главная асинхронная точка входа в программу
async function main() {
    activateThemesToggle();
    const vote = await api.getVote(getDeviceId());
    if (vote) {
        await loadLangsList()
    } else {
        await loadVoteForm()
    }
    console.log('App was successfully initialized')
}

//Погнали!)
main();

